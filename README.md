# Front-end Notebook - HTML5 e CSS3 - Tutorial DevEd

Projeto desenvolvido seguindo o tutorial do Youtuber Dev Ed (https://www.youtube.com/channel/UClb90NQQcskPUGDIXsQEz5Q).

**Conceitos aplicados:**
* Estilização de texto com fontes externas;
* Utilização de tags do HTML5 (main, article, section...);
* Listas UL;
* Display flex;
* Unidade de medidas vh e vw;
* Gradient text;
* Criação e estilização de botão;
* Sombra em imagem;
* Animação de imagem com keyframes;
* Design responsivo com media queries;

**Ferramentas utilizadas:**

* Visual Studio Code Versão 1.54.1;
* Navegador Google Chrome Versão 89.0.4389.82;
* Windows 10 Versão 1909 (OS Build 18363.1379);

**Para visualizar as funcionalidades do site, baixe todos os arquivos em uma mesma pasta e abra o arquivo index.html no seu navegador. Se preferir, na pasta Apresentação Site tem algumas screenshots da página.**


